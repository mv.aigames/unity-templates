﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engine : MonoBehaviour
{
    [SerializeField]
    private float _thrust;
    public float thrust {
        get { return _thrust; }
        set {
            if (value >= 0f)
            {
                _thrust = value;
            }
        }
    }

    private float speed = 0;

    public float GetSpeed(float force)
    {
        return 10f * force;
    }
}
