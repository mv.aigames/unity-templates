﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Racing
{
    [RequireComponent(typeof(Rigidbody), typeof(Engine))]
    public class Car : MonoBehaviour
    {
        private Engine engine;
        private Rigidbody rb;

        [SerializeField]
        private Vector3 facing = Vector3.forward;

        private float engineSpeed = 0f;

        private void Awake()
        {
            engine = GetComponent<Engine>();
            rb = GetComponent<Rigidbody>();
        }

        void FixedUpdate()
        {
            rb.AddForce(facing * engineSpeed);
        }

        /// <summary>
        /// -1 to 1
        /// </summary>
        /// <param name="value"></param>
        public void Turn(float value)
        {
            facing = Quaternion.AngleAxis(value * 10f * Time.deltaTime, Vector3.up) * facing;
        }



        /// <summary>
        /// 0 to 1
        /// </summary>
        /// <param name="value"></param>
        public void Accelerate(float value)
        {
            engineSpeed = engine.GetSpeed(value);
        }

        /// <summary>
        /// 0 to 1
        /// </summary>
        /// <param name="value"></param>
        public void Stop(float value)
        {

        }

        /// <summary>
        /// 0 to 1
        /// </summary>
        /// <param name="value"></param>
        public void GoBack(float value)
        {

        }
    }
}
