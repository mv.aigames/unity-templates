﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Racing
{
    public class PlayerController : MonoBehaviour
    {
        public Car car;
        
        void Update()
        {
            float v = Input.GetAxis("Vertical");
            car.Accelerate(v);

            float h = Input.GetAxis("Horizontal");
            car.Turn(h);
        
        }
    }
}
