﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS
{
    public class Civilian : MonoBehaviour
    {
        Unit unit;
        Timer timer;
        Vector3 target;
        bool waiting = true;

        private void Awake()
        {
            unit = GetComponent<Unit>();
            timer = GetComponent<Timer>();
            timer.Now();
            target = transform.position;
        }

        // Update is called once per frame
        void Update()
        {
            if (!waiting)
            {
                Vector3 diff = target - transform.position;
                diff.y = 0f;
                if (diff.magnitude < 1f)
                {
                    print("Goal reached...");
                    timer.Now();
                    waiting = true;
                }
                else
                {
                    unit.Move(diff.normalized);
                }
            }
        }

        private void OnTimeout()
        {
            print("Time out!");
            target = GenerateRandomTarget();
            waiting = false;
        }

        private Vector3 GenerateRandomTarget()
        {
            Vector3 v = transform.position;
            v += Random.onUnitSphere * Random.Range(1f, 10f);
            v.y = 0f;
            return v;
        }
    }
}
