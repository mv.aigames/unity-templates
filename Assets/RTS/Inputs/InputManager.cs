﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

namespace RTS
{
    public class InputManager : MonoBehaviour
    {

        Camera mainCamera;
        GameUI gameUI;

        private void Awake()
        {
            var ims = FindObjectsOfType<InputManager>();
            if (ims.Length > 1)
            {
                Destroy(gameObject);
                return;
            }
            
            DontDestroyOnLoad(this);
        }

        private void Start()
        {
            mainCamera = Camera.main;
            gameUI = FindObjectOfType<GameUI>();
        }

        void Update()
        {
            ProcessUserInput();
        }

        void ProcessUserInput()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out hit))
                {
                    if (gameUI)
                    {
                        Unit unit = hit.transform.GetComponent<Unit>();
                        if (unit)
                        {
                            gameUI.ShowUnit(unit);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Called when the player click on the mini map
        /// </summary>
        public void MoveCamera()
        { 

        }
    }
}
