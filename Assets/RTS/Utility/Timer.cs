﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField]
    private float time = 1f;

    [SerializeField]
    private bool oneShoot = true;

    private float timeLeft = 0f;

    public void Now()
    {
        print("Starting timer...");
        StartCoroutine("DecreaseTime");
    }

    public void Stop()
    {
        StopCoroutine("DecreaseTime");
    }

    private IEnumerator DecreaseTime()
    {
        timeLeft = time;

        while (timeLeft >= 0f)
        {
            timeLeft -= Time.deltaTime;
            yield return null;
        }

        SendMessage("OnTimeout", SendMessageOptions.DontRequireReceiver);
    }
}
