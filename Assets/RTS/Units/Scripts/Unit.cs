﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace RTS
{
    public enum UnitType
    {
        Civilian,
        Warrior,
    }

    [RequireComponent(typeof(CharacterController))]
    public class Unit : MonoBehaviour
    {
        [SerializeField]
        private float health = 100;

        [SerializeField]
        private float speed = 30f;

        [SerializeField]
        private float gravityForce = 9.8f;

        CharacterController cc;

        Vector3 gravity;

        private void Awake()
        {
            gravity = Physics.gravity;
            cc = GetComponent<CharacterController>();
        }

        void FixedUpdate()
        {
            
        }

        /// <summary>
        /// This method should be called in FixedUpdate.
        /// </summary>
        /// <param name="dir"></param>
        public void Move(Vector3 dir)
        {
            cc.Move(dir * speed * Time.fixedDeltaTime);
        }


    }
}
